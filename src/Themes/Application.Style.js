import colors from './Colors';
import {Dimensions, Platform, StyleSheet} from 'react-native';
import {fontFamily, fontRoot, fontSize} from './Constants';

const ApplicationStyle = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: colors.bgRoot,
  },
  toolbar: {
    flexDirection: 'row',
    width: '100%',
    height: Platform.OS === 'android' ? 48 : isIphoneX() ? 88 : 66,
    paddingTop: Platform.OS === 'android' ? 0 : isIphoneX() ? 40 : 24,
    backgroundColor: colors.cyan_blue,
    alignItems: 'center',
  },
  viewRowContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  viewWrapTitleToolbar: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleToolbar: {
    color: colors.white,
    fontFamily: fontRoot.bold,
    fontSize: fontSize.large,
  },
  viewWrapIcLeft: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icLeft: {
    width: 23,
    height: 23,
    tintColor: colors.white,
  },
  viewWrapIcRight: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icCenter: {
    width: 23,
    height: 23,
    justifyContent: 'center',
    alignItems: 'center',
    tintColor: colors.white,
  },
  icRight: {
    width: 23,
    height: 23,
    tintColor: colors.white,
  },
  textRight: {
    color: colors.white,
    fontFamily: fontRoot.medium,
    fontSize: fontSize.medium,
  },
  viewHorizontalLine: {
    backgroundColor: colors.grey,
    height: 0.5,
    alignSelf: 'stretch',
  },
  textInput: {
    height: 40,
    borderColor: colors.silver,
    borderBottomWidth: StyleSheet.hairlineWidth,
    marginBottom: 20,
  },
  form: {
    flex: 1,
    justifyContent: 'center',
    width: '80%',
  },

  catology: {
    flexDirection: 'row',
    height: 50,
  },

  catology_new: {
    flexDirection: 'row',
    height: 200,
  },

  title_catology: {
    flex: 2,
    backgroundColor: '#F5F5F5',
    margin: 2,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
  },

  title_date_catology: {
    flex: 1,
    backgroundColor: '#F5F5F5',
    margin: 2,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ApplicationStyle;

export function isIphoneX() {
  const dim = Dimensions.get('window');

  return (
    // This has to be iOS
    Platform.OS === 'ios' &&
    // Check either, iPhone X or XR
    (isIPhoneXSize(dim) || isIPhoneXrSize(dim))
  );
}

export function isIPhoneXSize(dim) {
  return dim.height === 812 || dim.width === 812;
}

export function isIPhoneXrSize(dim) {
  return dim.height === 896 || dim.width === 896;
}
