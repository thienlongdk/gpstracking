import colors from './Colors';

export const barStyle = {
  darkContent: 'dark-content',
  lightContent: 'light-content',
};

export const fontSize = {small: 14, medium: 16, large: 18};

export const fontFamily = {
  bold: 'iCielVAGRoundedNext-Bold',
  regular: 'iCielVAGRoundedNext-Regular',
  medium: 'iCielVAGRoundedNext-Medium',
  light: 'iCielVAGRoundedNext-Light',
  demiBold: 'iCielVAGRoundedNext-DemiBold',
  lightItalic: 'iCielVAGRoundedNext-LightItalic',
};

export const fontRoot = {
  // bold: 'HelveticaNeueBold',
  heavy: 'HelveticaNeueHeavy',
  italic: 'HelveticaNeueItalic',
  light: 'HelveticaNeueLight',
  // medium: 'HelveticaNeueMedium',
  regular: 'HelveticaNeue-Regular',
  thin: 'HelveticaNeueThin',
};

export const enumPriority = [
  {
    id: 0,
    title: '',
    color: colors.black,
  },
  {
    id: 1,
    title: 'Thấp',
    color: colors.asbestos,
  },
  {
    id: 2,
    title: 'Bình thường',
    color: colors.belize_hole,
  },
  {
    id: 3,
    title: 'Cao',
    color: colors.sun_flower,
  },
  {
    id: 4,
    title: 'Gấp',
    color: colors.carrot,
  },
  {
    id: 5,
    title: 'Tức thì',
    color: colors.pomegranate,
  },
];

export function getTitlePriority(id) {
  if (id !== undefined && id !== null) {
    return enumPriority[id].title;
  } else {
    return '';
  }
}

export function getColorPriority(id) {
  console.log('ID color: ', id);
  if (id !== undefined && id !== null) {
    return enumPriority[id].color;
  } else {
    return colors.black;
  }
}

export const enumMonth = [
  {
    id: '01',
    title: 'Tháng 1',
  },
  {
    id: '02',
    title: 'Tháng 2',
  },
  {
    id: '03',
    title: 'Tháng 3',
  },
  {
    id: '04',
    title: 'Tháng 4',
  },
  {
    id: '05',
    title: 'Tháng 5',
  },
  {
    id: '06',
    title: 'Tháng 6',
  },
  {
    id: '07',
    title: 'Tháng 7',
  },
  {
    id: '08',
    title: 'Tháng 8',
  },
  {
    id: '09',
    title: 'Tháng 9',
  },
  {
    id: '10',
    title: 'Tháng 10',
  },
  {
    id: '11',
    title: 'Tháng 11',
  },
  {
    id: '12',
    title: 'Tháng 12',
  },
];

export const ANDROID_VERSION = 30;
export const IOS_VERSION = 29;
