const images = {
  logoVNPT: require('../Images/VNPT.png'),
  drop: require('../Images/drop.png'),
  arrow_drop_down: require('../Images/arrow_drop_down.png'),
  arrow_drop_up: require('../Images/arrow_drop_up.png'),
  email: require('../Images/email.png'),
  password: require('../Images/password.png'),
  key: require('../Images/key.png'),
  loading_gif: require('../Images/loading.gif'),
  star_filled: require('../Images/star_filled.png'),
  star_unfilled: require('../Images/star_unfilled.png'),
  calendar: require('../Images/iconfinder_calendar_285670.png'),
  khasxkd: require('../Images/khasxkd.png'),
  CSS: require('../Images/logo.png'),
};

export default images;
