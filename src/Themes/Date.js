//Get week number total in current year
//Error
export function getAllWeekNumberOfYear(y) {
  d = new Date(Date.UTC(y, 11, 31));
  var dayNum = d.getUTCDay() || 7;
  d.setUTCDate(d.getUTCDate() + 4 - dayNum);
  var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
  return Math.ceil(((d - yearStart) / 86400000 + 1) / 7);
}

//Get week number total in current year (version 2.0)
function getWeekNumber(d) {
  // Copy date so don't modify original
  d = new Date(+d);
  d.setHours(0, 0, 0);
  // Set to nearest Thursday: current date + 4 - current day number
  // Make Sunday's day number 7
  d.setDate(d.getDate() + 4 - (d.getDay() || 7));
  // Get first day of year
  var yearStart = new Date(d.getFullYear(), 0, 1);
  // Calculate full weeks to nearest Thursday
  var weekNo = Math.ceil(((d - yearStart) / 86400000 + 1) / 7);
  // Return array of year and week number
  return [d.getFullYear(), weekNo];
}

//Get week number total in current year (version 2.0)
export function getWeekNumberNo(d) {
  // Copy date so don't modify original
  d = new Date(+d);
  d.setHours(0, 0, 0);
  // Set to nearest Thursday: current date + 4 - current day number
  // Make Sunday's day number 7
  d.setDate(d.getDate() + 4 - (d.getDay() || 7));
  // Get first day of year
  var yearStart = new Date(d.getFullYear(), 0, 1);
  // Calculate full weeks to nearest Thursday
  var weekNo = Math.ceil(((d - yearStart) / 86400000 + 1) / 7);
  // Return array of year and week number
  return weekNo;
}

export function weeksInYear(year) {
  var month = 11,
    day = 31,
    week;

  // Find week that 31 Dec is in. If is first week, reduce date until
  // get previous week.
  do {
    d = new Date(year, month, day--);
    week = getWeekNumber(d)[1];
  } while (week == 1);

  return week;
}

//Get 7 day in week with weekNo and year
function getDateStartOfISOWeek(w, y) {
  var simple = new Date(y, 0, 1 + (w - 1) * 7);
  var dow = simple.getDay();
  var ISOweekStart = simple;
  if (dow <= 4) {
    ISOweekStart.setDate(simple.getDate() - 7 - simple.getDay() + 1);
  } else {
    ISOweekStart.setDate(simple.getDate() - 7 + 8 - simple.getDay());
  }
  return ISOweekStart;
}

const WEEK_vi = ['T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'CN'];

export function getListDateOfWeekVN(w, y) {
  // var dd = new Date(getListDateOfWeek(13,2020)[0]);
  // console.log(dd.getDate() + '/' + (dd.getMonth() + 1) + '/' + dd.getFullYear() )
  var d = new Date();
  d = getDateStartOfISOWeek(w, y);
  var array = [];
  let tempArray = {
    day: WEEK_vi[0],
    date: new Date(d.setDate(d.getDate())),
  };
  array.push(tempArray);
  for (let i = 1; i < 7; i++) {
    let tempArray = {
      day: WEEK_vi[i],
      date: new Date(d.setDate(d.getDate() + 1)) ,
    };
    array.push(tempArray);
  }
  return array;
}
