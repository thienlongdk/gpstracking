import apisauce from 'apisauce';
var qs = require('qs');

export const api = apisauce.create({
  baseURL: 'http://14.225.245.238:3368/',
});

export const speed = 1000;

export const getMarkerAPI = payload => {
  return api.get(
    'gis/getpoint?lng=' +
      payload.lng +
      '&lat=' +
      payload.lat +
      '&radius=1000&token=' +
      payload.token,
    null,
    {
      //timeout: 10000,
      // headers: {
      //   Authorization:
      //     payload.getUser.token_type + ' ' + payload.getUser.access_token,
      // },
    },
  );
};

export const getTokenApp = payload => {
  return api.get(
    'gis/getId?token=' +
      payload.tokenFB +
      '&os=' +
      payload.os +
      '&device=' +
      payload.device,
    null,
    {
      //timeout: 10000,
      // headers: {
      //   Authorization:
      //     payload.getUser.token_type + ' ' + payload.getUser.access_token,
      // },
    },
  );
};

export const saveDiemAPI = (lat, lng, speed, token, bks) => {
  return api.get(
    'gis/savepoint?lng=' +
      lng +
      '&lat=' +
      lat +
      '&speed=' +
      speed +
      '&token=' +
      token +
      '&bks=' +
      bks,
  );
};
