import {all} from 'redux-saga/effects';
import {watchHandleLstMarker} from './Containers/Main/Main.Saga';

export default function* rootSaga() {
  yield all([watchHandleLstMarker()]);
}
