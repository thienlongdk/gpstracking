import React, {Component} from 'react';
import {Text, View, ActivityIndicator, Dimensions} from 'react-native';
import colors from '../../Themes/Colors';
export default class ReloadPageNoWhiteScreen extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          width: Dimensions.get('window').width,
        }}>
        <ActivityIndicator
          size="large"
          color={colors.cyan_blue}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        />
      </View>
    );
  }
}
