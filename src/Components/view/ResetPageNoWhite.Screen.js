import React, {Component} from 'react';
import {
  Text,
  View,
  ActivityIndicator,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import colors from '../../Themes/Colors';
export default class ResetPageNoWhiteScreen extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          width: Dimensions.get('window').width,
        }}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column',
          }}>
          <Text>Không có dữ liệu</Text>
          <TouchableOpacity
            style={{
              // flex: 1,
              backgroundColor: 'white',
              borderColor: colors.cyan_blue,
              borderWidth: 2,
              borderRadius: 4,
            }}
            onPress={this.props.onPressResetPage}>
            <Text style={{fontSize: 13, margin: 3}}>Tải lại trang</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
