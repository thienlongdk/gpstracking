import React, {Component} from 'react';
import {Text, View, ActivityIndicator, Dimensions} from 'react-native';
import colors from '../../Themes/Colors';
export default class NoDataPageScreen extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          width: Dimensions.get('window').width,
          backgroundColor: 'white',
        }}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>{this.props.text}</Text>
        </View>
      </View>
    );
  }
}
