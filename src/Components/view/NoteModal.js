import React, {Component} from 'react';
import {
  Text,
  View,
  ActivityIndicator,
  Dimensions,
  TouchableOpacity,
  Platform,
  Modal,
  SafeAreaView,
} from 'react-native';
import WebView from 'react-native-webview';
import WebViewModalProvider, {WebViewModal} from 'react-native-webview-modal';
import {Button} from 'native-base';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

export default class NoteModal extends Component {
  constructor(props) {
    super(props);
    this.state = {data: this.props.data};
  }

  render() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        style={{
          width: DEVICE_WIDTH,
          height: DEVICE_HEIGHT,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'rgba(0,0,0,0.5)',
        }}>
        <View
          style={{
            width: DEVICE_WIDTH,
            height: DEVICE_HEIGHT,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'rgba(0,0,0,0.5)',
          }}
          onPress={() => console.log('aaaa')}>
          <View
            style={{
              justifyContent: 'flex-start',
              position: 'absolute',
              alignItems: 'center',
              paddingLeft: 10,
              paddingRight: 10,
              width: 300,
              height: 200,
              opacity: 1,
              backgroundColor: 'white',
              borderRadius: 9,
            }}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 19,
                color: 'black',
                marginTop: 16,
              }}>
              Thông tin chi tiết
            </Text>
            <WebViewModalProvider>
              <View>
                <SafeAreaView />
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 19,
                    color: 'black',
                    marginTop: 16,
                  }}>
                  Thông tin chi tiết
                </Text>
                <Button title="Open" />
                <WebViewModal
                  visible={true}
                  source={{uri: 'https://google.com'}}
                />
              </View>
            </WebViewModalProvider>
          </View>
        </View>
      </Modal>
    );
  }
}
