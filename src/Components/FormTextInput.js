import * as React from 'react';
import {StyleSheet, TextInput, TextInputProps, View} from 'react-native';
import colors from '../Themes/Colors';
import images from '../Themes/Images';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

type Props = TextInputProps;

class FormTextInput extends React.Component<Props> {
  constructor(props) {
    super(props);
    // create a ref to store the textInput DOM element
    this.textInputRef = React.createRef();
    this.focus = this.focus.bind(this);
  }

  focus = () => {
    if (this.textInputRef.current) {
      this.textInputRef.current.focus();
    }
  };

  render() {
    const {style, ...otherProps} = this.props;

    return (
      <View style={styles.viewTextInput}>
        <View style={styles.viewTextInputContainer}>
          <MaterialCommunityIcons
            name={this.props.icon}
            size={20}
            color={colors.white}
            style={styles.image_text_input}
          />
          <TextInput
            ref={this.textInputRef}
            selectionColor={colors.DODGER_BLUE}
            placeholderTextColor="white"
            style={[styles.textInput, style]}
            {...otherProps}
          />
        </View>
        <View style={styles.viewUnderLine} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    height: 40,
    marginLeft: 10,
    color: colors.white,
    flex: 1,
  },
  image_text_input: {
    marginTop: 10,
  },
  viewTextInput: {
    height: 41,
    opacity: 1.0,
    marginTop: 10,
  },
  viewTextInputContainer: {
    flexDirection: 'row',
  },
  viewUnderLine: {
    height: 1,
    backgroundColor: 'white',
  },
});

export default FormTextInput;
