export function fixDataDichVuHangThang(lstDTHangThang) {
  lstDTHangThang.datasets.map((value) => {
    var colorStringTemp: string = value.color;
    value.color = (opacity = 1) => getColor1(colorStringTemp);
  });
  return lstDTHangThang;
}

export function fixDataDichVuToiThangHienTai(lstDTToiThangHienTai) {
  var a = [];
  lstDTToiThangHienTai.datasets[0].colors.map((value) => {
    var colorStringTemp: string = value;
    a.push((opacity = 1) => getColor1(colorStringTemp));
  });
  lstDTToiThangHienTai.datasets[0].colors = a;
  return lstDTToiThangHienTai;
}

export function getColor1(value: string): string {
  return value;
}
