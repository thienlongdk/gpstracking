import {ANDROID_VERSION, IOS_VERSION} from '../Themes/Constants';
import SimpleToast from 'react-native-simple-toast/index';
import {Alert, Linking} from 'react-native';

let reg = /^[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;

export function checkEmail(email) {
  return reg.test(email);
}

export function checkUpdateVersion(data, platform): boolean {
  let check: boolean = false;
  if (data !== null && data !== undefined && data.toString().trim() !== '') {
    if (platform === 'android') {
      data.map((key) => {
        if (
          key.STR_KEY === 'VERSION_ANDROID_NUM' &&
          key.NUM_VALUE > ANDROID_VERSION
        ) {
          check = true;
        }
      });
    } else {
      data.map((key) => {
        if (key.STR_KEY === 'VERSION_IOS_NUM' && key.NUM_VALUE > IOS_VERSION) {
          check = true;
        }
      });
    }
  } else {
    check = false;
  }
  return check;
}

export function clickUpdateVersion(data, platform) {
  if (data !== null && data !== undefined && data.toString().trim() !== '') {
    if (platform === 'android') {
      data.map((key) => {
        if (key.STR_KEY === 'LINK_VERSION_ANDROID') {
          Linking.openURL(key.STR_VALUE);
        }
      });
    } else {
      data.map((key) => {
        if (key.STR_KEY === 'LINK_VERSION_IOS') {
          Linking.openURL(key.STR_VALUE);
        }
      });
    }
  } else {
    SimpleToast.show('Hiện tại chưa thể cập nhật ứng dụng.');
  }
}

export function checkRoleAPIs(getInfo, Controller, Action): Boolean {
  let quyen = getInfo.data.data !== null ? getInfo.data.data.roles : [];

  let subscription = quyen.filter(
    (e) =>
      e.Name === Controller && e.Action === Action && e.ModuleName === 'APIs',
  );
  if (JSON.stringify(subscription) === '[]') {
    return false;
  } else {
    return true;
  }
}

export function checkDateOutOfDate(dateComplete, dateNow): Boolean {
  dateComplete.setHours(0, 0, 0, 0);
  dateNow.setHours(0, 0, 0, 0);
  if (dateComplete.getTime() < dateNow.getTime()) {
    return true;
  } else {
    return false;
  }
}
