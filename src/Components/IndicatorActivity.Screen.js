import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Animated,
  Easing,
  AsyncStorage,
  Alert,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {Picker} from '@react-native-community/picker';
export default class IndicatorActivityScreen extends Component {
  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <ActivityIndicator size="large" color="#0000ff" />
        {this.props.title ? (
          <Text style={{color: 'black', textAlign: 'center', marginTop: 10}}>
            {this.props.title}
          </Text>
        ) : (
          <></>
        )}
      </View>
    );
  }
}
