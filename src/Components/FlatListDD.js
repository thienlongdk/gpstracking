import React, {Component} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Image,
  StatusBar,
  TextInput,
  TouchableOpacity,
  SafeAreaView,
  View,
  StyleSheet,
  Group,
  Text,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
//import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {connect} from 'react-redux';
// import styles from '../Login/Login.Style';
// import images from '../../Themes/Images';
import colors from '../Themes/Colors';
import strings from '../Themes/Strings';
import FormTextInput from './FormTextInput';
import {ChosenDD} from '../Containers/LichCongTac/LichCongTac.Action';

class FlatListDD extends Component {
  constructor(props) {
    super(props);
    let {datads, dsid} = this.props.getdataContacts;
    //  console.log(nvlct);
    this.state = {
      ds_idnv: dsid,
      dsnv: datads,
      selectedItems: [],
      arr: [],
    };
  }

  addThisMenber = () => {
    const {item} = this.props;
    // console.log(item.id);
    this.props.onCallApi(ChosenDD(item));

    //==================================
  };

  render() {
    // const nhanvien =  ? 'line-through' : 'none';
    // const {nhanViens} = this.props.getdataContacts.con;
    let {dsnv, ds_idnv} = this.state;
    // console.log(ds_idnv);
    //  alert(JSON.stringify(nhanViens));
    const {Ten_DD, Dia_Chi} = this.props.item;
    // const [ten, dv] = name.split('-');

    // console.log(ten);
    // console.log('aaaaaa')
    // console.log(dv);
    return (
      <View style={{flex: 1}}>
        <TouchableOpacity
          style={{
            backgroundColor: colors.white,
            // borderBottomColor: 'green',
            // borderBottomWidth: 1,
            paddingTop: 8,
            height: 62,
            borderRadius: 1,
            borderBottomColor: 'black',
            // paddingBottom: 7,
            marginHorizontal: 6,
            borderBottomWidth: 1,
            flexDirection: 'row',
          }}
          onPress={() => this.addThisMenber()}>
          <View style={styles1.flatListItem}>
            <Text style={{color: 'black', fontSize: 16}}>{Ten_DD}</Text>
            <Text
              style={{
                color: '#858585',
                // padding: 1,
                // marginLeft: -3,
                fontSize: 13,
              }}>
              {Dia_Chi}
            </Text>
          </View>

          {/* <Text style={styles1.flatListItem}></Text> */}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles1 = StyleSheet.create({
  flatListItem: {
    color: 'black',
    // padding: 12,
    // paddingHorizontal: 2,
    // marginHorizontal: -7,
    paddingBottom: 3,
    // paddingTop: 4,
    fontSize: 16,
    flex: 1,
  },
});
const mapStateToProps = (state) => {
  return {
    getdataContacts: state.getContacts,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onCallApi: (object) => dispatch(object),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FlatListDD);
