import React, {Component} from 'react';
import {
  Modal,
  TouchableWithoutFeedback,
  Text,
  StyleSheet,
  Platform,
  View,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Picker} from '@react-native-community/picker';
import images from '../Themes/Images';

export default class FormPickerCustom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
    this.setModalVisible = this.setModalVisible.bind(this);
  }

  setModalVisible = (visible) => {
    console.log('aaaaa');
    this.setState({modalVisible: visible});
  };

  render() {
    if (Platform.OS === 'android') {
      return (
        <View style={styles.view_picker}>
          <Picker
            style={[styles.picker, this.props.styleWidth]}
            selectedValue={this.props.value}
            onValueChange={(itemValue, itemIndex) =>
              this.props.onValueChange(itemValue, itemIndex)
            }>
            {this.props.items.map((i, index) => (
              <Picker.Item key={index} label={i.label} value={i.value} />
            ))}
          </Picker>
        </View>
      );
    } else {
      const selectedItem = this.props.items.find(
        (i) => i.value === this.props.value,
      );
      const selectedLabel = selectedItem ? selectedItem.label : '';
      return (
        <View style={styles.container}>
          <TouchableOpacity onPress={() => this.setState({modalVisible: true})}>
            <View style={[styles.input, this.props.styleWidth]}>
              <Text style={{fontSize: 15}}>{selectedLabel}</Text>
              <View>
                <Image style={{width: 17, height: 17}} source={images.drop} />
              </View>
            </View>
          </TouchableOpacity>
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => this.setModalVisible(false)}>
            <TouchableWithoutFeedback
              onPress={() => this.setState({modalVisible: true})}>
              <View style={styles.modalContainer}>
                <TouchableOpacity
                  style={styles.buttonContainer}
                  onPress={() => this.setState({modalVisible: false})}>
                  <Text
                    style={{
                      color: 'blue',
                    }}>
                    Done
                  </Text>
                </TouchableOpacity>
                <View>
                  <Picker
                    style={{backgroundColor: '#ececec'}}
                    selectedValue={this.props.value}
                    onValueChange={(itemValue, itemIndex) =>
                      this.props.onValueChange(itemValue, itemIndex)
                    }>
                    {this.props.items.map((i, index) => (
                      <Picker.Item
                        key={index}
                        label={i.label}
                        value={i.value}
                      />
                    ))}
                  </Picker>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 5,
    alignSelf: 'stretch',
    justifyContent: 'center',
  },
  input: {
    paddingHorizontal: 7,
    paddingVertical: 10,
    borderWidth: 1,
    borderRadius: 7,
    color: 'black',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  modalContainer: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  buttonContainer: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    padding: 20,
    backgroundColor: '#ececec',
  },

  view_picker: {
    backgroundColor: 'white',
    marginLeft: 7,
    borderRadius: 7,
    marginTop: Platform.OS === 'android' ? 10 : 0,
    marginBottom: 10,
    borderWidth: 1,
    height: 40,
  },

  picker: {
    color: 'black',
    height: 40,
  },
});
