export function formatNumber(amount): String {
  return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}
