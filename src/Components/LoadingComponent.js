import * as React from 'react';
import { ActivityIndicator, View, Text} from 'react-native';
import colors from '../Themes/Colors';

class LoadingComponent extends React.Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator size="large" color={colors.cyan_blue} />
        <View style={{marginTop: 30}}>
          <Text>Dữ liệu đang tải xuống</Text>
        </View>
      </View>
    );
  }
}

export default LoadingComponent;
