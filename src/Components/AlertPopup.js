import React, {Component} from 'react';
import {
  Modal,
  TouchableWithoutFeedback,
  Text,
  StyleSheet,
  Platform,
  View,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Picker} from '@react-native-community/picker';
import images from '../Themes/Images';

export default class AlertPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
    this.setModalVisible = this.setModalVisible.bind(this);
  }

  setModalVisible = (visible) => {
    console.log('aaaaa');
    this.setState({modalVisible: visible});
  };

  render() {
    if (Platform.OS === 'android') {
      return (
        <View style={styles.view_picker}>
          <Picker
            style={[styles.picker, this.props.styleWidth]}
            selectedValue={this.props.value}
            onValueChange={this.props.onValueChange}>
            {this.props.items.map((i, index) => (
              <Picker.Item key={index} label={i.label} value={i.value} />
            ))}
          </Picker>
        </View>
      );
    } else {
      const selectedItem = this.props.items.find(
        (i) => i.value === this.props.value,
      );
      const selectedLabel = selectedItem ? selectedItem.label : '';
      return (
        <View style={styles.container}>
          <TouchableOpacity onPress={() => this.setState({modalVisible: true})}>
            <View style={[styles.input, this.props.styleWidth]}>
              <Text style={{fontSize: 15}}>{selectedLabel}</Text>
              <View>
                <Image style={{width: 17, height: 17}} source={images.drop} />
              </View>
            </View>
          </TouchableOpacity>
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => this.setModalVisible(false)}>
            <TouchableWithoutFeedback
              onPress={() => this.setState({modalVisible: true})}>
              <View style={styles.modalContainer}>
                <View style={styles.buttonContainer}>
                  <Text
                    style={{color: 'blue'}}
                    onPress={() => this.setState({modalVisible: false})}>
                    Done
                  </Text>
                </View>
                <View>
                  <Picker
                    style={{backgroundColor: '#ececec'}}
                    selectedValue={this.props.value}
                    onValueChange={(itemValue, itemIndex) =>
                      this.props.onValueChange(itemValue, itemIndex)
                    }>
                    {this.props.items.map((i, index) => (
                      <Picker.Item
                        key={index}
                        label={i.label}
                        value={i.value}
                      />
                    ))}
                  </Picker>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    flex: 1,
  },
  content: {
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 5,
    alignSelf: 'stretch',
    justifyContent: 'center',
  },
  input: {
    padding: 7,
    borderWidth: 1,
    borderRadius: 7,
    color: 'black',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    marginRight: 9,
  },
  modalContainer: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  buttonContainer: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    padding: 20,
    backgroundColor: '#ececec',
  },

  view_picker: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: 7,
    marginTop: Platform.OS === 'android' ? 10 : 0,
    marginBottom: 10,
    marginRight: Platform.OS === 'android' ? 10 : 0,
    borderWidth: 1,
  },

  picker: {
    color: 'black',
    height: 40,
  },
});
