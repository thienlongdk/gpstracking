import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  TouchableHighlight,
  Dimensions,
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../Themes/Colors';

const MonthYearPicker = (props) => {
  const month_data = [
    {key: '01', name: 'Tháng 1'},
    {key: '02', name: 'Tháng 2'},
    {key: '03', name: 'Tháng 3'},
    {key: '04', name: 'Tháng 4'},
    {key: '05', name: 'Tháng 5'},
    {key: '06', name: 'Tháng 6'},
    {key: '07', name: 'Tháng 7'},
    {key: '08', name: 'Tháng 8'},
    {key: '09', name: 'Tháng 9'},
    {key: '10', name: 'Tháng 10'},
    {key: '11', name: 'Tháng 11'},
    {key: '12', name: 'Tháng 12'},
  ];

  const {width, height} = Dimensions.get('window');

  const [month, setMonth] = useState(month_data[new Date().getMonth()]);
  const [year, setYear] = useState(new Date().getFullYear());

  useEffect(() => {
    props.onChangeYear(year);
    props.onChangeMonth(month_data[new Date().getMonth()]);
  });

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={props.isShow}
      onRequestClose={props.close}>
      <TouchableHighlight
        style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.3)'}}
        onPress={props.close}>
        <View />
      </TouchableHighlight>
      <View style={{flex: 2 / 3, backgroundColor: 'white'}}>
        <View style={styles.yearContainer}>
          <TouchableOpacity
            onPress={() => {
              setYear(year - 1);
              props.onChangeYear(year - 1);
            }}>
            <MaterialCommunityIcons
              name={'chevron-left'}
              size={35}
              color={colors.black}
            />
          </TouchableOpacity>
          <Text style={styles.yearLabel}>{year}</Text>
          <TouchableOpacity
            onPress={() => {
              setYear(year + 1);
              props.onChangeYear(year + 1);
            }}>
            <MaterialCommunityIcons
              name={'chevron-right'}
              size={35}
              color={colors.black}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.monthContainer}>
          {month_data.map((item, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => {
                setMonth(item);
                props.onChangeMonth(item);
              }}
              style={[
                styles.month,
                {
                  width: width / 3,
                  backgroundColor:
                    item.key == month.key ? colors.belize_hole : 'white',
                },
              ]}>
              <Text style={{color: item.key == month.key ? 'white' : 'black'}}>
                {item.name}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
    </Modal>
  );
};

const styles = {
  yearContainer: {
    padding: 15,
    height: 55,
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  monthContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
  },

  month: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },

  yearLabel: {
    fontWeight: 'bold',
    fontSize: 25,
  },
};

export default MonthYearPicker;
