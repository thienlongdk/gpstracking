import {Dimensions, StyleSheet} from 'react-native';
import {fontFamily, fontSize, fontRoot} from '../../Themes/Constants';
import ApplicationStyle from '../../Themes/Application.Style';
import colors from '../../Themes/Colors';

export default StyleSheet.create({
  ...ApplicationStyle,
  container: {
    ...StyleSheet.absoluteFillObject,
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
    alignItems: 'center',
  },
  buttonRegister: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.dodger_blue,
    marginTop: 20,
    marginBottom: 20,
    paddingVertical: 10,
    borderRadius: 4,
    borderColor: 'rgba(255,255,255,0.7)',
  },
  textButton: {
    color: colors.white,
    textAlign: 'center',
    fontFamily: fontRoot.medium,
    height: 20,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
