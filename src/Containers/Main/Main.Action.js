export const HANDLE_GET_TOKEN = 'HANDLE_GET_TOKEN';
export const HANDLE_LST_MARKER = 'HANDLE_LST_MARKER';
export const LST_MARKER_SUCCESS = 'LST_MARKER_SUCCESS';
export const LST_MARKER_FAILURE = 'LST_MARKER_FAILURE';
export const SAVE_TOKEN_FIREBASE = 'SAVE_TOKEN_FIREBASE';
export const SAVE_TOKEN_APP = 'SAVE_TOKEN_APP';
export const SAVE_BIEN_SO_XE = 'SAVE_BIEN_SO_XE';

export const HANDLE_LST_SAVE_MARKER = 'HANDLE_LST_SAVE_MARKER';
export const LST_SAVE_MARKER_SUCCESS = 'LST_SAVE_MARKER_SUCCESS';
export const LST_SAVE_MARKER_FAILURE = 'LST_SAVE_MARKER_FAILURE';

export const actionLstMarker = (lat, lng, radius, token) => {
  return {type: HANDLE_LST_MARKER, payload: {lat, lng, radius, token}};
};

export const actionGetToken = (tokenFB, os, device) => {
  return {type: HANDLE_GET_TOKEN, payload: {tokenFB, os, device}};
};

export const lstMarkerSuccess = data => {
  return {type: LST_MARKER_SUCCESS, payload: {data}};
};

export const lstMarkerFailure = err => {
  return {type: LST_MARKER_FAILURE, payload: {err}};
};

export const saveTokenFirebase = token => {
  return {type: SAVE_TOKEN_FIREBASE, payload: {token}};
};

export const saveToken = token => {
  return {type: SAVE_TOKEN_APP, payload: {token}};
};

export const saveBienSoXe = bienSoXe => {
  return {type: SAVE_BIEN_SO_XE, payload: {bienSoXe}};
};

export const actionLstSaveMarker = (lat, lng, radius, token, bks) => {
  return {
    type: HANDLE_LST_SAVE_MARKER,
    payload: {lat, lng, radius, token, bks},
  };
};

export const lstSaveMarkerSuccess = data => {
  return {type: LST_SAVE_MARKER_SUCCESS, payload: {data}};
};

export const lstSaveMarkerFailure = err => {
  return {type: LST_SAVE_MARKER_FAILURE, payload: {err}};
};
