import React, {Component} from 'react';
import {
  Alert,
  Dimensions, PermissionsAndroid,
  Platform,
  SafeAreaView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import styles from './Main.Style';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import {getDistance, getPreciseDistance} from 'geolib';
import {
  actionGetToken,
  actionLstMarker,
  actionLstSaveMarker,
  lstMarkerFailure,
  lstMarkerSuccess,
  saveBienSoXe,
  saveToken,
  saveTokenFirebase,
} from './Main.Action';
import messaging, {firebase} from '@react-native-firebase/messaging';
import SimpleToast from 'react-native-simple-toast';
import colors from '../../Themes/Colors';
import {getSaveLstMarker} from './Main.Reducer';
import {saveDiemAPI} from '../../Services/Api';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

class MainScreen extends Component {
  constructor(props) {
    super(props);
    var region = {};
    Geolocation.getCurrentPosition(position => {
      region = {
        latitude: parseFloat(position.coords.latitude),
        longitude: parseFloat(position.coords.longitude),
        latitudeDelta: 5,
        longitudeDelta: 5,
      };
    });
    this.state = {
      initialRegion: region,
      startRegion: region,
      nowRegion: null,
      endRegion: null,
      getLstMarker: {fetching: false, data: null, err: null},
      notifiedMarker: 0,
      bienSoXe: this.props.bienSoXe,
      checkBienSoXe: this.props.bienSoXe != '' ? true : false,
      getListSaveDiem: {Title: ''},
      checkNotify: false,
    };
    this.getCurrentLocation();
    this.handleBienSoXeChange = this.handleBienSoXeChange.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.getLstMarker !== prevState.getLstMarker) {
      return {
        getLstMarker: nextProps.getLstMarker,
      };
    }
    return null;
  }

  async componentDidMount() {
    //Call Firebase
    try {
      firebase.messaging().requestPermission();

      messaging()
        .registerDeviceForRemoteMessages()
        .then(() => {
          messaging()
            .getToken()
            .then(fcmToken => {
              if (fcmToken) {
                this.props.onCallApi(saveTokenFirebase(fcmToken));
              }
            })
            .catch(error => {
              console.log('[FCMService] getToken rejected ', error);
            });
        });
    } catch (error) {
      alert('permission rejected');
    }
    if (Platform.OS === 'ios') {
      await Geolocation.requestAuthorization('whenInUse');
    }

    if (Platform.OS === 'android') {
      await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );
    }
    //Get ID
    this.props.onCallApi(
      actionGetToken(this.props.rdToken, 'iOS', this.state.bienSoXe),
    );
    this.getMarket();
    this.interval = setInterval(() => {
      Geolocation.watchPosition(
        position => {
          console.log(
            'Speed: ',
            position.coords.speed,
            position.coords.latitude,
            position.coords.longitude,
          );
          let pTemp = getPreciseDistance(this.state.startRegion, {
            latitude: parseFloat(position.coords.latitude),
            longitude: position.coords.longitude,
          });

          //if (position.coords.speed >= 50 || pTemp >= 100) {
          if (pTemp >= 100) {
            saveDiemAPI(
              parseFloat(position.coords.latitude),
              parseFloat(position.coords.longitude),
              100,
              this.props.tokenApp,
              this.props.bienSoXe,
            ).then(res => {
              console.log('REssss: ', res);
              if (res.ok) {
                if (res.data.success) {
                  if (this.state.nowRegion == null) {
                    this.setState({getListSaveDiem: res.data});
                    Geolocation.watchPosition(position => {
                      this.setState({
                        nowRegion: {
                          latitude: parseFloat(position.coords.latitude),
                          longitude: parseFloat(position.coords.longitude),
                        },
                        startRegion: {
                          latitude: parseFloat(position.coords.latitude),
                          longitude: parseFloat(position.coords.longitude),
                        },
                        checkNotify: true,
                      });
                    });

                    setTimeout(() => {
                      this.setState({checkNotify: false});
                    }, 5000);
                  } else {
                    //check dieu kien
                    Geolocation.watchPosition(position => {
                      this.setState({
                        nowRegion: {
                          latitude: parseFloat(position.coords.latitude),
                          longitude: parseFloat(position.coords.longitude),
                        },
                      });
                      let p = {
                        latitude: res.data.Lat,
                        longitude: res.data.Lng,
                      };

                      let x1_p = getPreciseDistance(this.state.startRegion, p);

                      let x_p = getPreciseDistance(this.state.nowRegion, p);

                      if (res.data.Lat > 0 && x1_p < x_p) {
                        this.setState({
                          startRegion: {
                            latitude: parseFloat(position.coords.latitude),
                            longitude: parseFloat(position.coords.longitude),
                          },
                          checkNotify: true,
                        });
                        setTimeout(() => {
                          this.setState({checkNotify: false});
                        }, 5000);
                      }
                    });
                  }
                } else {
                  this.setState({checkNotify: false});
                }
              }
            });
          }
        },
        error => SimpleToast.show('Không thể lấy toạ độ hiện tại.'),
        {
          enableHighAccuracy: true,
          timeout: 20000,
          maximumAge: 1000,
        },
      );
    }, 10000);
  }

  getCurrentLocation = () => {
    Geolocation.getCurrentPosition(position => {
      let region = {
        latitude: parseFloat(position.coords.latitude),
        longitude: parseFloat(position.coords.longitude),
        latitudeDelta: 5,
        longitudeDelta: 5,
      };
      this.setState(
        {
          initialRegion: region,
          startRegion: region,
        },
        error => console.log(error),
        {
          enableHighAccuracy: true,
          timeout: 20000,
          maximumAge: 1000,
        },
      );
    });
  };

  getMarket = () => {
    Geolocation.getCurrentPosition(
      position => {
        SimpleToast.show('Speed: ' + position.coords.speed.toString());
        this.props.onCallApi(
          actionLstMarker(
            parseFloat(position.coords.latitude),
            parseFloat(position.coords.longitude),
            1000,
            this.props.tokenApp,
          ),
        );
      },
      error => SimpleToast.show('Không thể lấy toạ độ hiện tại.'),
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000,
      },
    );
  };

  goToInitialLocation = () => {
    let initialRegion = Object.assign({}, this.state.initialRegion);
    initialRegion.latitudeDelta = 0.005;
    initialRegion.longitudeDelta = 0.005;
    this.mapView.animateToRegion(initialRegion, 2000);
  };

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  handleBienSoXeChange = value => {
    this.setState({bienSoXe: value});
  };

  handleLuuBienSoXePress = () => {
    if (this.state.bienSoXe != '') {
      this.props.onCallApi(saveBienSoXe(this.state.bienSoXe));
      this.props.onCallApi(
        actionGetToken(
          this.props.rdToken != null ? this.props.rdToken : '',
          'iOS',
          this.state.bienSoXe,
        ),
      );
      this.setState({checkBienSoXe: true});
    } else {
      SimpleToast.show('Hãy nhận biển số xe của bạn.');
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.checkNotify ? (
          <View
            style={{
              position: 'absolute',
              width: '100%',
              height: 70,
              backgroundColor: 'white',
              zIndex: 999999,
              flexDirection: 'row',
              paddingTop: 20,
              paddingHorizontal: 10,
            }}>
            <MaterialCommunityIcons
              name={'bell-ring'}
              size={35}
              color={'blue'}
            />
            <View style={{marginTop: 10, marginLeft: 10}}>
              <Text>{this.state.getListSaveDiem.Title}</Text>
            </View>
          </View>
        ) : (
          <></>
        )}
        {this.state.checkBienSoXe ? (
          <MapView
            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
            style={styles.map}
            region={this.state.region}
            followUserLocation={true}
            ref={ref => (this.mapView = ref)}
            zoomEnabled={true}
            showsUserLocation={true}
            onMapReady={this.goToInitialLocation.bind(this)}
            initialRegion={this.state.initialRegion}>
            {this.state.getLstMarker.data !== undefined &&
            this.state.getLstMarker.data !== null ? (
              this.state.getLstMarker.data.recordset !== undefined &&
              this.state.getLstMarker.data.recordset !== null ? (
                this.state.getLstMarker.data.recordset.map((marker, index) => (
                  <Marker
                    key={index}
                    coordinate={{latitude: marker.Lat, longitude: marker.Lng}}
                    title={marker.Title}
                  />
                ))
              ) : (
                <></>
              )
            ) : (
              <></>
            )}
          </MapView>
        ) : (
          <View
            style={{
              width: Dimensions.get('window').width,
              paddingHorizontal: 16,
            }}>
            <Text>Nhập biển số xe</Text>
            <TextInput
              ref={this.textInputRef}
              selectionColor={colors.DODGER_BLUE}
              placeholderTextColor="black"
              style={styles.textInput}
              value={this.state.bienSoXe}
              onChangeText={this.handleBienSoXeChange}
              returnKeyType="next"
              autoCapitalize="none"
            />
            <View style={styles.viewUnderLine} />
            <TouchableOpacity
              style={styles.buttonRegister}
              onPress={this.handleLuuBienSoXePress}>
              <Text style={styles.textButton}>Lưu</Text>
            </TouchableOpacity>
          </View>
        )}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    getLstMarker: state.getLstMarker,
    bienSoXe: state.bienSoXe,
    rdToken: state.rdToken,
    tokenApp: state.tokenApp,
    getSaveLstMarker: state.getSaveLstMarker,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onCallApi: object => dispatch(object),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
