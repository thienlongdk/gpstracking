import {call, put, takeLatest} from 'redux-saga/effects';
import {
  HANDLE_LST_MARKER,
  HANDLE_GET_TOKEN,
  HANDLE_LST_SAVE_MARKER,
  lstMarkerSuccess,
  lstMarkerFailure,
  lstSaveMarkerSuccess,
  lstSaveMarkerFailure,
  saveToken,
} from './Main.Action';
import {sendNetworkFail} from '../../actions';
import SimpleToast from 'react-native-simple-toast';
import {getMarkerAPI, getTokenApp, saveDiemAPI} from '../../Services/Api';
import Geolocation from 'react-native-geolocation-service';

export function* watchHandleLstMarker() {
  yield takeLatest(HANDLE_GET_TOKEN, handleGetToken);
  yield takeLatest(HANDLE_LST_MARKER, handleLstMarker);
  yield takeLatest(HANDLE_LST_SAVE_MARKER, handleLstSaveMarker);
}

function* handleGetToken(action) {
  const response = yield call(getTokenApp, action.payload);
  if (response.ok) {
    yield put(
      saveToken(response.data.token != undefined ? response.data.token : null),
    );
  } else {
    if (
      response.problem !== 'NETWORK_ERROR' &&
      response.problem !== 'TIMEOUT_ERROR' &&
      response.problem !== 'CONNECTION_ERROR'
    ) {
      SimpleToast.show('Kiểm tra kết nối mạng');
    } else {
      yield put(sendNetworkFail(response.problem));
    }
  }
}

function* handleLstMarker(action) {
  const response = yield call(getMarkerAPI, action.payload);
  if (response.ok) {
    yield put(lstMarkerSuccess(response.data));
  } else {
    if (
      response.problem !== 'NETWORK_ERROR' &&
      response.problem !== 'TIMEOUT_ERROR' &&
      response.problem !== 'CONNECTION_ERROR'
    ) {
      SimpleToast.show('Kiểm tra kết nối mạng');
      yield put(lstMarkerFailure(response.problem));
    } else {
      yield put(sendNetworkFail(response.problem));
      yield put(lstMarkerFailure(response.problem));
    }
  }
}

function* handleLstSaveMarker(action) {
  const response = yield call(saveDiemAPI, action.payload);
  if (response.ok) {
    yield put(lstSaveMarkerSuccess(response.data));
  } else {
    if (
      response.problem !== 'NETWORK_ERROR' &&
      response.problem !== 'TIMEOUT_ERROR' &&
      response.problem !== 'CONNECTION_ERROR'
    ) {
      SimpleToast.show('Kiểm tra kết nối mạng');
      yield put(lstSaveMarkerFailure(response.problem));
    } else {
      yield put(sendNetworkFail(response.problem));
      yield put(lstSaveMarkerFailure(response.problem));
    }
  }
}
