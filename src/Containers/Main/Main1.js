import React, {Component} from 'react';
import {Alert, View} from 'react-native';
import {connect} from 'react-redux';
import styles from './Main.Style';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import {getDistance, getPreciseDistance} from 'geolib';
import {actionLstMarker} from './Main.Action';

class MainScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initialRegion: null,
      startRegion: null,
      getLstMarker: {fetching: false, data: null, err: null},
      notifiedMarker: 0,
    };
    this.getCurrentLocation();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.getLstMarker !== prevState.getLstMarker) {
      console.log(
        'getLstMarker getDerivedStateFromProps: ',
        nextProps.getLstMarker,
      );
      return {getLstMarker: nextProps.getLstMarker};
    }
    return null;
  }

  componentDidMount() {
    this.props.onCallApi(actionLstMarker(0, 0, 0, 'aa'));
    this.interval = setInterval(() => {
      if (this.state.startRegion == null) {
        this.getCurrentLocation();
      } else {
        Geolocation.getCurrentPosition(
          position => {
            let endRegion = {
              latitude: parseFloat(position.coords.latitude),
              longitude: parseFloat(position.coords.longitude),
              latitudeDelta: 5,
              longitudeDelta: 5,
            };
            var pdis = getPreciseDistance(this.state.startRegion, endRegion);
            if (pdis > 100) {
              this.props.onCallApi(
                actionLstMarker(
                  parseFloat(position.coords.latitude),
                  parseFloat(position.coords.longitude),
                  pdis,
                  'aa',
                ),
              );
              this.setState({
                startRegion: endRegion,
              });
            }
            if (
              this.state.getLstMarker.data !== undefined &&
              this.state.getLstMarker.data !== null
            ) {
              if (
                this.state.getLstMarker.data.recordset !== undefined &&
                this.state.getLstMarker.data.recordset !== null
              ) {
                this.state.getLstMarker.data.recordset.map((marker, index) => {
                  var newpdis = getPreciseDistance(this.state.startRegion, {
                    latitude: marker.Lat,
                    longitude: marker.Lng,
                  });
                  console.log('newpdis: ', newpdis);
                  if (newpdis < 200) {
                    if (this.state.notifiedMarker !== marker.id) {
                      this.setState({notifiedMarker: marker.id});
                      Alert.alert(
                        'Thông báo',
                        `Bạn sắp tới địa điểm ${marker.Title}`,
                        [
                          {
                            text: 'Đóng',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                          },
                        ],
                        {cancelable: false},
                      );
                    }
                  }
                });
              }
            }
          },
          error => console.log(error),
          {
            enableHighAccuracy: true,
            timeout: 20000,
            maximumAge: 1000,
          },
        );
      }
    }, 10000);
  }

  // onRegionChange(region) {
  //   this.setState({region: region});
  // }

  getCurrentLocation = () => {
    Geolocation.getCurrentPosition(
      position => {
        let region = {
          latitude: parseFloat(position.coords.latitude),
          longitude: parseFloat(position.coords.longitude),
          latitudeDelta: 5,
          longitudeDelta: 5,
        };
        this.setState({
          initialRegion: region,
          startRegion: region,
        });
      },
      error => console.log(error),
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000,
      },
    );
  };

  goToInitialLocation = () => {
    let initialRegion = Object.assign({}, this.state.initialRegion);
    initialRegion.latitudeDelta = 0.005;
    initialRegion.longitudeDelta = 0.005;
    this.mapView.animateToRegion(initialRegion, 2000);
  };

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <View style={styles.container}>
        <MapView
          provider={PROVIDER_GOOGLE} // remove if not using Google Maps
          style={styles.map}
          region={this.state.region}
          followUserLocation={true}
          ref={ref => (this.mapView = ref)}
          zoomEnabled={true}
          showsUserLocation={true}
          onMapReady={this.goToInitialLocation.bind(this)}
          initialRegion={this.state.initialRegion}>
          {this.state.getLstMarker.data !== undefined &&
          this.state.getLstMarker.data !== null ? (
            this.state.getLstMarker.data.recordset !== undefined &&
            this.state.getLstMarker.data.recordset !== null ? (
              this.state.getLstMarker.data.recordset.map((marker, index) => (
                <Marker
                  key={index}
                  coordinate={{latitude: marker.Lat, longitude: marker.Lng}}
                  title={marker.Title}
                />
              ))
            ) : (
              <></>
            )
          ) : (
            <></>
          )}
        </MapView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    getLstMarker: state.getLstMarker,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onCallApi: object => dispatch(object),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
