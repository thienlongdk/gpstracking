import {
  HANDLE_LST_MARKER,
  LST_MARKER_SUCCESS,
  LST_MARKER_FAILURE,
  SAVE_TOKEN_FIREBASE,
  SAVE_TOKEN_APP,
  SAVE_BIEN_SO_XE,
  HANDLE_LST_SAVE_MARKER,
  LST_SAVE_MARKER_SUCCESS,
  LST_SAVE_MARKER_FAILURE,
} from './Main.Action';
import _ from 'lodash';

const initialState = {fetching: false, data: null, err: null};

const arrayUnion = (arr1, arr2, identifier) => {
  const array = [...arr1, ...arr2];

  return _.uniqBy(array, identifier);
};

export const getLstMarker = (state = initialState, action) => {
  console.log('Login Reducer ------ action: ', action);
  switch (action.type) {
    case HANDLE_LST_MARKER:
      return state;
    case LST_MARKER_SUCCESS:
      if (state.data == null) {
        return {
          fetching: true,
          data: action.payload.data,
          err: null,
        };
      } else {
        if (
          action.payload.data.recordset !== undefined &&
          action.payload.data.recordset !== null
        ) {
          state.data.recordset = arrayUnion(
            state.data.recordset,
            action.payload.data.recordset,
            'id',
          );
          return state;
        } else {
          return state;
        }
      }
    case LST_MARKER_FAILURE:
      return {
        fetching: false,
        data: state.data,
        err: action.payload.err,
      };
    default:
      return state;
  }
};

const initialStateToken = '';

export const rdToken = (state = initialStateToken, action) => {
  console.log('Login Reducer ------ action: ', action);
  switch (action.type) {
    case SAVE_TOKEN_FIREBASE:
      return action.payload.token;
    default:
      return state;
  }
};

export const tokenApp = (state = null, action) => {
  console.log('Login Reducer ------ action: ', action);
  switch (action.type) {
    case SAVE_TOKEN_APP:
      return action.payload.token;
    default:
      return state;
  }
};

export const bienSoXe = (state = initialStateToken, action) => {
  console.log('Login Reducer ------ action: ', action);
  switch (action.type) {
    case SAVE_BIEN_SO_XE:
      return action.payload.bienSoXe;
    default:
      return state;
  }
};

export const getSaveLstMarker = (state = initialState, action) => {
  console.log('Login Reducer ------ action: ', action);
  switch (action.type) {
    case HANDLE_LST_SAVE_MARKER:
      return {
        fetching: true,
        data: null,
        err: null,
      };
    case LST_SAVE_MARKER_SUCCESS:
      console.log('Login Reducer ------ LOGIN_SUCCESS: ', {
        fetching: false,
        data: action.payload.data,
        err: null,
      });
      return {
        fetching: true,
        data: action.payload.data,
        err: null,
      };
    case LST_SAVE_MARKER_FAILURE:
      return {
        fetching: false,
        data: null,
        err: action.payload.err,
      };
    default:
      return state;
  }
};

