import React, {Component} from 'react';
import {connect} from 'react-redux';
import styles from './RootContainer.Style';
import {Keyboard, Platform, View} from 'react-native';
import {clearNetworkFail} from '../../actions';
import Toast from 'react-native-simple-toast';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import MainScreen from '../Main/Main.Screen';
import {saveToken} from '../Main/Main.Action';
import messaging, {firebase} from '@react-native-firebase/messaging';
const Stack = createNativeStackNavigator();

class RootContainerScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isKeyboardShow: false,
      keyboardHeight: 0,
      isShowNetworkErr: false,
    };
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this.keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.keyboardDidHide,
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.sendNetworkFail.err) {
      switch (nextProps.sendNetworkFail.err) {
        case 'NETWORK_ERROR':
          Toast.show('Không có mạng internet, vui lòng kiểm tra lại!');
          break;
        case 'Network Error':
          Toast.show('Không có mạng internet, vui lòng kiểm tra lại!');
          break;
        case 'TIMEOUT_ERROR':
          Toast.show('Đường truyền mạng không ổn định, vui lòng kiểm tra lại!');
          break;
        case 'CONNECTION_ERROR':
          Toast.show('Lỗi DNS server, vui lòng kiểm tra lại');
          break;
        case 'CLIENT_ERROR':
          Toast.show('Thông tin gửi lên không chính xác.');
          break;
        case 'Request failed with status code 404':
          Toast.show('Lỗi gửi dữ liệu lên server, vui lòng kiểm tra lại');
          break;
        default:
          Toast.show(nextProps.sendNetworkFail.err);
          break;
      }
      nextProps.onCallApi(clearNetworkFail());
    }
    return null;
  }

  keyboardDidShow = e => {
    this.setState({
      isKeyboardShow: true,
      keyboardHeight: e.endCoordinates.height,
    });
  };

  keyboardDidHide = () => {
    this.setState({isKeyboardShow: false});
  };

  render() {
    return (
      <View style={styles.mainContainer}>
        <NavigationContainer>
          <Stack.Navigator
            headerMode={'none'}
            screenOptions={{
              headerShown: false,
            }}>
            <Stack.Screen name="MainScreen" component={MainScreen} />
          </Stack.Navigator>
        </NavigationContainer>
        {this.state.isKeyboardShow && Platform.OS === 'ios' ? (
          <View style={{height: this.state.keyboardHeight}} />
        ) : null}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    getUser: state.getUser,
    sendNetworkFail: state.sendNetworkFail,
    isCheckOTP: state.isCheckOTP,
    getInfo: state.getInfo,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onCallApi: object => dispatch(object),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RootContainerScreen);
